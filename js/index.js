$(document).ready(function () {
  var lastScrollTop = 0;
  $(window).scroll(function(event) {
    var pos_body = $('html,body').scrollTop();
    if (pos_body < 2264) {
      $('.btn-scroll').hide()
    } else {
      $('.btn-scroll').show()
    }
    if (pos_body < 66.4000015258789) {
      $('.btn-telegram').hide()
    } else {
      $('.btn-telegram').show()
    }
    lastScrollTop = pos_body
  });
  $('.btn-scroll').on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
  });
  $('.open-tool-bar').on('click', function () {
    $(this).hide();
    $('.close-tool-bar').show();
    $(".menu-nar").show('2000');
  })
  $('.close-tool-bar').on('click', function () {
    $(this).hide();
    $('.open-tool-bar').show();
    $(".menu-nar").hide('1500');
  })
  $('.btn-item-menu').on('click', function () {
    $('.close-tool-bar').hide();
    $('.open-tool-bar').show();
    $(".menu-nar").hide('1500');
  })
});
